Source: liblinux-pid-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               perl-xs-dev,
               perl:native
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liblinux-pid-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liblinux-pid-perl.git
Homepage: https://metacpan.org/release/Linux-Pid
Testsuite: autopkgtest-pkg-perl

Package: liblinux-pid-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: wrapper around the getpid() and getppid() C functions
 Perl already returns the PID and PPID in variables and builtins. Linux::Pid
 forces perl to call the underlying C functions getpid() and getppid().
 .
 This is useful with multithreaded programs. Linux' C library, using the Linux
 thread model, returns different values of the PID and the PPID from different
 threads.
 .
 A known consumer of this functionality is Apache2::SizeLimit (in
 libapache2-mod-perl2).
